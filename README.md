1、使用jxls制定Excel模板
2、远程服务提供数据源
3、中间层，串接Excel模板和远程数据

整个架构思想：解耦Excel模板和数据源，增强扩展性。
技术点：
1、根据数据库中配置的返回数据的字段(template_dataobject_library表中的数据)，运行期动态生成实体类
2、从返回的数据json串中，获取指定列表Key的数据(列表key由template_dataobject_catalog中data_response_key配置)
3、Excel模板中的数据变量由数据库配置(template_dataobject_catalog表中的data_key)
4、远程数据请求地址由数据库配置(template_dataobject_catalog表中的remote_request_url)
5、commons-jexl版本是org.apache.commons:commons-jexl3:3.1，这个版本设置函数的方式有所调整
6、服务化思想，模块与模块之间高度解耦，增强扩展性



数据库表设计：
表名：template_dataobject_catalog(模板请求数据配置)
serialno                                --流水号
template_id                             --模板ID
data_key                                --excel模板中数据对象对应的key，填充数据使用
data_response_key                       --远程返回数据中，列表数据对应的key，比如：lists
transfer_type                           --返回数据转换类型:Bean;Map(目前实现的是Bean，运行期动态生成实体类)
remote_request_url                      --远程数据源请求地址
remote_request_type                     --请求类型:GET,POST(目前实现的是POST)
create_time                             --创建时间
update_time                             --更新时间


表名：template_dataobject_library
serialno                                --流水号
template_id                             --模板ID
data_type                               --入参:request;出参:response
colKey                                  --字段
colKeyName                              --字段描述
keyClassType                            --字段的全限定类型
sort                                    --排序(主要用于入参,dataType为request时,保证参数顺序)
create_time                             --创建时间
update_time                             --更新时间