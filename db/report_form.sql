/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : report_form

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-12-23 01:11:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `template_dataobject_catalog`
-- ----------------------------
DROP TABLE IF EXISTS `template_dataobject_catalog`;
CREATE TABLE `template_dataobject_catalog` (
  `serialno` varchar(50) NOT NULL,
  `template_id` varchar(100) NOT NULL,
  `data_key` varchar(100) NOT NULL COMMENT 'excel模板中数据对象对应的key',
  `data_response_key` varchar(100) NOT NULL COMMENT '远程返回数据中列表数据对应的key',
  `transfer_type` varchar(50) NOT NULL COMMENT '返回数据转换类型:Bean;Map',
  `remote_request_url` varchar(500) NOT NULL COMMENT '远程数据源请求地址',
  `remote_request_type` varchar(50) NOT NULL COMMENT '请求类型:GET,POST',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`serialno`),
  UNIQUE KEY `INDEX_TEMPLATE_ID` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of template_dataobject_catalog
-- ----------------------------
INSERT INTO `template_dataobject_catalog` VALUES ('1', 'object_collection_template', 'employees', 'lists', 'Bean', 'http://127.0.0.1:9999/report-form/request', 'POST', '2019-12-22 16:05:30', '2019-12-22 16:05:33');

-- ----------------------------
-- Table structure for `template_dataobject_library`
-- ----------------------------
DROP TABLE IF EXISTS `template_dataobject_library`;
CREATE TABLE `template_dataobject_library` (
  `serialno` varchar(50) NOT NULL,
  `template_id` varchar(100) NOT NULL,
  `data_type` varchar(50) NOT NULL COMMENT '入参:request;出参:response',
  `colKey` varchar(50) NOT NULL COMMENT '字段',
  `colKeyName` varchar(50) NOT NULL COMMENT '字段描述',
  `keyClassType` varchar(200) NOT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '排序(主要用于入参,dataType为request时,保证参数顺序)',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`serialno`),
  KEY `INDEX_TEMPLATE_ID` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of template_dataobject_library
-- ----------------------------
INSERT INTO `template_dataobject_library` VALUES ('1', 'object_collection_template', 'response', 'name', '姓名', 'java.lang.String', null, '2019-12-22 16:06:56', '2019-12-22 16:06:59');
INSERT INTO `template_dataobject_library` VALUES ('2', 'object_collection_template', 'response', 'salary', '薪水', 'java.math.BigDecimal', null, '2019-12-22 16:07:59', '2019-12-22 16:08:01');
INSERT INTO `template_dataobject_library` VALUES ('3', 'object_collection_template', 'response', 'age', '年龄', 'java.lang.Integer', null, '2019-12-22 16:08:33', '2019-12-22 16:08:36');
