package com.zyong.report.jxls.util;

import java.util.List;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class JsonTest {
    private static String json = "{\n" +
            "    \"responseCode\": \"000000\",\n" +
            "    \"responseMsg\": \"成功\",\n" +
            "    \"lists\": [\n" +
            "        {\n" +
            "            \"name\": \"张勇1\",\n" +
            "            \"salary\": 10001.1,\n" +
            "            \"age\": 21\n" +
            "        },\n" +
            "        {\n" +
            "            \"name\": \"张勇2\",\n" +
            "            \"salary\": 10003.1,\n" +
            "            \"age\": 22\n" +
            "        },\n" +
            "        {\n" +
            "            \"name\": \"张勇3\",\n" +
            "            \"salary\": 10007.1,\n" +
            "            \"age\": 24\n" +
            "        },\n" +
            "        {\n" +
            "            \"name\": null,\n" +
            "            \"salary\": null,\n" +
            "            \"age\": null\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    private static String json2 = "{\n" +
            "    \"responseCode\": \"000000\",\n" +
            "    \"responseMsg\": \"成功\",\n" +
            "    \"data\": {\n" +
            "\t\t\"lists\": [\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": \"张勇1\",\n" +
            "\t\t\t\t\"salary\": 10001.1,\n" +
            "\t\t\t\t\"age\": 21\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": \"张勇2\",\n" +
            "\t\t\t\t\"salary\": 10003.1,\n" +
            "\t\t\t\t\"age\": 22\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": \"张勇3\",\n" +
            "\t\t\t\t\"salary\": 10007.1,\n" +
            "\t\t\t\t\"age\": 24\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": null,\n" +
            "\t\t\t\t\"salary\": null,\n" +
            "\t\t\t\t\"age\": null\n" +
            "\t\t\t}\n" +
            "\t\t]\n" +
            "\t}\n" +
            "}";

    public static void main(String[] args) {
//        JSONObject jsonObject = JSON.parseObject(json);
        JSONObject jsonObject = JSON.parseObject(json2);

        List<JSONArray> list = new ArrayList<>();
        JsonUtil.getJsonArrayValue(jsonObject, "lists", list);

        System.out.println("list:" + list);
    }
}
