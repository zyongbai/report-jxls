package com.zyong.report.jxls.controller;

import com.zyong.report.jxls.pojo.RemoteCallBase;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import com.zyong.report.jxls.service.RemoteDataTransferExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "report")
public class ReportJxlsController {
    @Autowired
    private RemoteDataTransferExcel remoteDataTransferExcel;

    @RequestMapping(value = "request/{templateId}", method = RequestMethod.GET)
    @ResponseBody
    public String dataTransfer(@PathVariable("templateId") String templateId) {
        RemoteCallBase remoteCallBase = new RemoteCallBase();
        remoteDataTransferExcel.transferRemoteDataToExcel(templateId, remoteCallBase);

        return "true";
    }
}
