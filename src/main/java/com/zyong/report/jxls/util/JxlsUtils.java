package com.zyong.report.jxls.util;

import java.io.*;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import java.text.SimpleDateFormat;
import org.jxls.transform.Transformer;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlBuilder;
import org.jxls.expression.JexlExpressionEvaluator;

/**
 * jxls Excel模板操作工具类
 */
public class JxlsUtils {
    /** jxls模板目录 */
    private static final String TEMPLATE_PATH = "jxls-template";

    /**
     * 根据Excel模板,生成最终的Excel报表
     * @param is
     * @param os
     * @param model
     * @throws IOException
     */
    public static void exportExcel(InputStream is, OutputStream os, Map<String, Object> model) throws IOException {
        Context context = new Context();
        if (model != null) {
            for (String key : model.keySet()) {
                context.putVar(key, model.get(key));
            }
        }

        JxlsHelper jxlsHelper = JxlsHelper.getInstance();
        Transformer transformer  = jxlsHelper.createTransformer(is, os);
        JexlExpressionEvaluator evaluator = (JexlExpressionEvaluator)transformer.getTransformationConfig().getExpressionEvaluator();

        // 添加自定义函数
        Map<String, Object> funcs = new HashMap<>();
        funcs.put("utils", new JxlsUtils());
        JexlEngine jexl = new JexlBuilder().namespaces(funcs).create();
        evaluator.setJexlEngine(jexl);

        jxlsHelper.processTemplate(context, transformer);
    }

    /**
     * 根据Excel模板,生成最终的Excel报表
     * @param xls
     * @param out
     * @param model
     * @throws IOException
     */
    public static void exportExcel(File xls, File out, Map<String, Object> model) throws IOException {
        exportExcel(new FileInputStream(xls), new FileOutputStream(out), model);
    }

    /**
     * 根据Excel模板,生成最终的Excel报表
     * @param templateName
     * @param os
     * @param model
     * @throws IOException
     */
    public static void exportExcel(String templateName, OutputStream os, Map<String, Object> model) throws IOException {
        File template = getTemplate(templateName);
        if (template != null) {
            exportExcel(new FileInputStream(template), os, model);
        }
    }

    /**
     * 获取jxls模版文件
     * @param name
     * @return
     */
    public static File getTemplate(String name){
        String templatePath = JxlsUtils.class.getClassLoader().getResource(TEMPLATE_PATH).getPath();
        File template = new File(templatePath, name);
        if (template.exists()) {
            return template;
        }
        return null;
    }

    /**
     * 日期格式化
     * @param date
     * @param fmt
     * @return
     */
    public String dateFmt(Date date, String fmt) {
        if (date == null) {
            return "";
        }
        try {
            SimpleDateFormat dateFmt = new SimpleDateFormat(fmt);
            return dateFmt.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * if判断
     * @param b
     * @param o1
     * @param o2
     * @return
     */
    public Object ifelse(boolean b, Object o1, Object o2) {
        return b ? o1 : o2;
    }

}
