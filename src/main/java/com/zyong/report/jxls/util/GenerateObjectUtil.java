package com.zyong.report.jxls.util;

import java.util.Map;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import net.sf.cglib.beans.BeanGenerator;
import org.apache.commons.beanutils.BeanUtils;
import com.zyong.report.jxls.pojo.TemplateDataObjectLibraryPojo;

/**
 * 实体工具类
 */
public class GenerateObjectUtil {
    /**
     * 根据属性动态生成对象，并赋值
     * <p>
     * 注意：返回生成的对象属性，均在设置属性前加入前缀$cglib_prop_，例如$cglib_prop_userId
     *
     * @param propertyMap Map<生成的对象变量名称，生成的对象变量值>
     * @return Object
     */
    public static Object generateObjectByField(Map<String, Object> propertyMap) throws Exception {
        BeanGenerator generator = new BeanGenerator();
        for (Map.Entry<String, Object> entry : propertyMap.entrySet()) {
            generator.addProperty(entry.getKey(), entry.getValue().getClass());
        }
        // 构建对象
        Object obj = generator.create();
        // 赋值
        for (Map.Entry<String, Object> en : propertyMap.entrySet()) {
            BeanUtils.setProperty(obj, en.getKey(), en.getValue());
        }

        // json转换去掉前缀
        String jsonString = JSON.toJSONString(obj).replace("$cglib_prop_", "");
        return JSON.parseObject(jsonString, Object.class);
    }

    /**
     * 根据模板与数据,创建对象
     * @param templateDataList
     * @param jsonObject
     * @return
     * @throws Exception
     */
    public static Object generateObjectByField(List<TemplateDataObjectLibraryPojo> templateDataList, JSONObject jsonObject) throws Exception {
        BeanGenerator generator = new BeanGenerator();
        for (TemplateDataObjectLibraryPojo templateDataPojo : templateDataList) {
            String keyClassType = templateDataPojo.getKeyClassType();
            Class keyClass = Class.forName(keyClassType);
            generator.addProperty(templateDataPojo.getColKey(), keyClass);
        }
        // 构建对象
        Object obj = generator.create();
        // 赋值
        for (TemplateDataObjectLibraryPojo templateDataPojo : templateDataList) {
            BeanUtils.setProperty(obj, templateDataPojo.getColKey(), jsonObject.getString(templateDataPojo.getColKey()));
        }

        // json转换去掉前缀
        String jsonString = JSON.toJSONString(obj).replace("$cglib_prop_", "");
        return JSON.parseObject(jsonString, Object.class);
    }
}
