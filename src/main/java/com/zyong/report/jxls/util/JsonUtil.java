package com.zyong.report.jxls.util;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class JsonUtil {
    public static void getJSONValue(JSONObject json, String k, List<String> list){
        for (Object j : json.keySet()){
            if (isJSONObj(json.get(j))) {
                // 是对象
                JSONObject j2 = JSON.parseObject(json.get(j).toString());
                getJSONValue(j2, k, list);
            } else if (isJSONArray(json.get(j))) {
                JSONArray j3= JSON.parseArray(json.get(j).toString());
                // 是数组
                getJSONValue(j3, k, list);
            } else if (j == k) {
                // 是字符串
                list.add(json.get(j).toString());
            }
        }
    }

    public static void getJSONValue(JSONArray json, String k, List<String> list){
        for (Object j : json) {
            if (isJSONObj(j)) {
                // 是对象
                JSONObject j2 = JSON.parseObject(j.toString());
                getJSONValue(j2,k,list);
            } else if (isJSONArray(j)) {
                // 是数组
                JSONArray j3 = JSON.parseArray(j.toString());
                getJSONValue(j3,k,list);
            }
        }
    }

    public static void getJsonArrayValue(JSONObject json, String k, List<JSONArray> list){
        for (Object j : json.keySet()){
            if (isJSONObj(json.get(j))) {
                // 是对象
                JSONObject j2 = JSON.parseObject(json.get(j).toString());
                getJsonArrayValue(j2, k, list);
            } else if (isJSONArray(json.get(j))) {
                // 是数组
                JSONArray j3 = JSON.parseArray(json.get(j).toString());
                if (k.equals(j)) {
                    list.add(j3);
                } else {
                    getJsonArrayValue(j3, k, list);
                }
            }
        }
    }

    public static void getJsonArrayValue(JSONArray json, String k, List<JSONArray> list){
        for (Object j : json) {
            if (isJSONObj(j)) {
                // 是对象
                JSONObject j2 = JSON.parseObject(j.toString());
                getJsonArrayValue(j2, k, list);
            } else if (isJSONArray(j)) {
                // 是数组
                JSONArray j3 = JSON.parseArray(j.toString());
                if (k.equals(j)) {
                    list.add(j3);
                } else {
                    getJsonArrayValue(j3, k, list);
                }
            }
        }
    }

    public static boolean isJSONObj(Object json){
        return json instanceof JSONObject;
    }

    public static boolean isJSONArray(Object json){
        return json instanceof JSONArray;
    }
}
