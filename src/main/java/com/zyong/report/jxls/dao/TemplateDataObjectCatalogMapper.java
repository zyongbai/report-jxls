package com.zyong.report.jxls.dao;

import java.util.Date;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Results;
import org.springframework.stereotype.Repository;
import com.zyong.report.jxls.pojo.TemplateDataObjectCatalogPojo;

@Repository
public interface TemplateDataObjectCatalogMapper {
    @Select("SELECT * FROM template_dataobject_catalog WHERE template_id = #{templateId}")
    @Results({ @Result(property = "serialNo", column = "serialno"), @Result(property = "templateId", column = "template_id"),
            @Result(property = "dataKey", column = "data_key"), @Result(property = "dataResponseKey", column = "data_response_key"),
            @Result(property = "transferType", column = "transfer_type"),
            @Result(property = "remoteRequestUrl", column = "remote_request_url"), @Result(property = "remoteRequestType", column = "remote_request_type"),
            @Result(property = "createTime", column = "create_time", javaType = Date.class), @Result(property = "updateTime", column = "update_time", javaType = Date.class)})
    TemplateDataObjectCatalogPojo selectTemplateCatalogById(String templateId);
}
