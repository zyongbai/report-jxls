package com.zyong.report.jxls.dao;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.springframework.stereotype.Repository;
import com.zyong.report.jxls.pojo.TemplateDataObjectLibraryPojo;

@Repository
public interface TemplateDataObjectLibraryMapper {
    @Select("SELECT * FROM template_dataobject_library WHERE template_id = #{templateId} and data_type = #{dataType} order by sort")
    @Results({ @Result(property = "serialNo", column = "serialno"), @Result(property = "templateId", column = "template_id"),
            @Result(property = "dataType", column = "data_type"), @Result(property = "colKey", column = "colKey"),
            @Result(property = "colKeyName", column = "colKeyName"), @Result(property = "keyClassType", column = "keyClassType"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "createTime", column = "create_time", javaType = Date.class), @Result(property = "updateTime", column = "update_time", javaType = Date.class)})
    List<TemplateDataObjectLibraryPojo> selectTemplateLibraryById(String templateId, String dataType);
}
