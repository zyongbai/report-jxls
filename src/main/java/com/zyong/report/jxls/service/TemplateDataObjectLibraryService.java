package com.zyong.report.jxls.service;

import java.util.List;
import com.zyong.report.jxls.pojo.TemplateDataObjectLibraryPojo;

/**
 * 模板入参出参字段配置
 */
public interface TemplateDataObjectLibraryService {
    /**
     * 根据模板ID和参数类型,获取所有字段信息
     * @param templateId
     * @param dataType
     * @return
     */
    List<TemplateDataObjectLibraryPojo> selectTemplateLibraryById(String templateId, String dataType);
}
