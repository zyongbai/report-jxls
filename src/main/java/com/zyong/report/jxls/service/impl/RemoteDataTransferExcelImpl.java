package com.zyong.report.jxls.service.impl;

import java.util.*;
import java.io.OutputStream;
import java.io.FileOutputStream;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zyong.report.jxls.common.Constants;
import com.zyong.report.jxls.util.JsonUtil;
import com.zyong.report.jxls.util.JxlsUtils;
import org.springframework.stereotype.Service;
import com.zyong.report.jxls.pojo.RemoteCallBase;
import com.zyong.report.jxls.util.GenerateObjectUtil;
import com.zyong.report.jxls.remote.RemoteCallService;
import com.zyong.report.jxls.service.RemoteDataTransferExcel;
import org.springframework.beans.factory.annotation.Autowired;
import com.zyong.report.jxls.pojo.TemplateDataObjectCatalogPojo;
import com.zyong.report.jxls.pojo.TemplateDataObjectLibraryPojo;
import com.zyong.report.jxls.service.TemplateDataObjectCatalogService;
import com.zyong.report.jxls.service.TemplateDataObjectLibraryService;
import org.springframework.util.StringUtils;

@Service
public class RemoteDataTransferExcelImpl implements RemoteDataTransferExcel {
    @Autowired
    private TemplateDataObjectCatalogService templateDataObjectCatalogService;
    @Autowired
    private TemplateDataObjectLibraryService templateDataObjectLibraryService;
    @Autowired
    private RemoteCallService remoteCallService;

    /**
     * {@inheritDoc}
     * @param templateId
     * @param remoteCallBase
     */
    @Override
    public void transferRemoteDataToExcel(String templateId, RemoteCallBase remoteCallBase) {
        // 1、根据模板ID获取数据请求配置信息
        TemplateDataObjectCatalogPojo templateDataObjectCatalogPojo =
                templateDataObjectCatalogService.selectTemplateCatalogById(templateId);

        if (templateDataObjectCatalogPojo == null || StringUtils.isEmpty(templateDataObjectCatalogPojo.getRemoteRequestType())) {
            throw new RuntimeException("数据请求配置信息或者请求类型不能为空");
        }

        // 2、远程调用,获取数据
        JSONObject jsonObject = null;
        String remoteRequestType = templateDataObjectCatalogPojo.getRemoteRequestType();
        if (Constants.REMOTE_REQUEST_TYPE_GET.equals(remoteRequestType)) {

        } else if (Constants.REMOTE_REQUEST_TYPE_POST.equals(remoteRequestType)) {
            jsonObject = remoteCallService.postRequestCall(templateDataObjectCatalogPojo, remoteCallBase);
        } else {
            throw new RuntimeException("不支持的请求类型");
        }

        if (jsonObject == null) {
            throw new RuntimeException("remote request call return null");
        }
        List<TemplateDataObjectLibraryPojo> templateResponseDataList =
                templateDataObjectLibraryService.selectTemplateLibraryById(templateId, Constants.DATA_TYPE_RESPONSE);

        List<JSONArray> jsonArrayList = new ArrayList<>();
        JsonUtil.getJsonArrayValue(jsonObject, templateDataObjectCatalogPojo.getDataResponseKey(), jsonArrayList);

        // 组装最终的数据对象
        List<Object> objectLists = new ArrayList<>();
        for (JSONArray jsonArray : jsonArrayList) {
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject tmpJson = jsonArray.getJSONObject(i);
                try {
                    Object obj = GenerateObjectUtil.generateObjectByField(templateResponseDataList, tmpJson);
                    objectLists.add(obj);
                } catch (Exception e) {
                    continue;
                }
            }
        }

        try {
            OutputStream os = new FileOutputStream("target/object_collection_output.xls");
            Map<String , Object> model=new HashMap<String , Object>();
            model.put(templateDataObjectCatalogPojo.getDataKey(), objectLists);
            model.put("nowdate", new Date());
            JxlsUtils.exportExcel("object_collection_template.xls", os, model);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
