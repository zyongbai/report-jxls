package com.zyong.report.jxls.service.impl;

import java.util.List;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Service;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import com.zyong.report.jxls.pojo.TemplateDataObjectLibraryPojo;
import com.zyong.report.jxls.dao.TemplateDataObjectLibraryMapper;
import com.zyong.report.jxls.service.TemplateDataObjectLibraryService;

/**
 * @date 2019/12/22
 */
@Service
public class TemplateDataObjectLibraryServiceImpl implements TemplateDataObjectLibraryService {
    @Autowired
    private TemplateDataObjectLibraryMapper templateDataObjectLibraryMapper;

    /**
     * {@inheritDoc}
     * @param templateId
     * @param dataType
     * @return
     */
    @Override
    public List<TemplateDataObjectLibraryPojo> selectTemplateLibraryById(String templateId, String dataType) {
        if (StringUtils.isEmpty(templateId)) {
            return Lists.newArrayList();
        }

        return templateDataObjectLibraryMapper.selectTemplateLibraryById(templateId, dataType);
    }
}
