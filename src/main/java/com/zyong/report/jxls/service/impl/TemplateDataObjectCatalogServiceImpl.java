package com.zyong.report.jxls.service.impl;

import org.springframework.util.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.zyong.report.jxls.pojo.TemplateDataObjectCatalogPojo;
import com.zyong.report.jxls.dao.TemplateDataObjectCatalogMapper;
import com.zyong.report.jxls.service.TemplateDataObjectCatalogService;

/**
 * @date 2019/12/22
 */
@Service
public class TemplateDataObjectCatalogServiceImpl implements TemplateDataObjectCatalogService {
    @Autowired
    private TemplateDataObjectCatalogMapper templateDataObjectCatalogMapper;

    /**
     * {@inheritDoc}
     * @param templateId
     * @return
     */
    @Override
    public TemplateDataObjectCatalogPojo selectTemplateCatalogById(String templateId) {
        if (StringUtils.isEmpty(templateId)) {
            return null;
        }

        return templateDataObjectCatalogMapper.selectTemplateCatalogById(templateId);
    }
}
