package com.zyong.report.jxls.service;

import com.zyong.report.jxls.pojo.RemoteCallBase;

public interface RemoteDataTransferExcel {
    /**
     * 根据模板ID和请求参数,获取远程数据,生成最终的Excel报表
     * @param templateId
     * @param remoteCallBase
     */
    void transferRemoteDataToExcel(String templateId, RemoteCallBase remoteCallBase);
}
