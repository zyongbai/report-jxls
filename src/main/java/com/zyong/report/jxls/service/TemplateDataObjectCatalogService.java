package com.zyong.report.jxls.service;

import com.zyong.report.jxls.pojo.TemplateDataObjectCatalogPojo;

/**
 * 模板数据请求配置
 */
public interface TemplateDataObjectCatalogService {
    /**
     * 根据模板ID获取数据请求配置信息
     * @param templateId
     * @return
     */
    TemplateDataObjectCatalogPojo selectTemplateCatalogById(String templateId);
}
