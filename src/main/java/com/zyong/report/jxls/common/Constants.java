package com.zyong.report.jxls.common;

public class Constants {
    /**
     * 请求类型:GET,POST
     */
    public static final String REMOTE_REQUEST_TYPE_GET = "GET";
    public static final String REMOTE_REQUEST_TYPE_POST = "POST";

    /**
     * 返回数据转换类型:Bean;Map
     */
    public static final String TRANSFER_TYPE_BEAN = "Bean";
    public static final String TRANSFER_TYPE_MAP = "Map";

    /**
     * 入参:request;出参:response
     */
    public static final String DATA_TYPE_REQUEST = "request";
    public static final String DATA_TYPE_RESPONSE = "response";
}
