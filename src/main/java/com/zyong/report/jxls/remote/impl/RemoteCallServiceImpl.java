package com.zyong.report.jxls.remote.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;
import com.zyong.report.jxls.pojo.RemoteCallBase;
import org.springframework.web.client.RestTemplate;
import com.zyong.report.jxls.remote.RemoteCallService;
import org.springframework.beans.factory.annotation.Autowired;
import com.zyong.report.jxls.pojo.TemplateDataObjectCatalogPojo;

@Service
public class RemoteCallServiceImpl implements RemoteCallService {
    @Autowired
    private RestTemplate restTemplate;

    /**
     * {@inheritDoc}
     * @param remoteCallBase
     * @return
     */
    @Override
    public JSONObject postRequestCall(TemplateDataObjectCatalogPojo templateDataObjectCatalogPojo, RemoteCallBase remoteCallBase) {
        if (remoteCallBase != null) {
            boolean onlySelfFlag = remoteCallBase.getClass().isAssignableFrom(RemoteCallBase.class);
            if (onlySelfFlag) {
                remoteCallBase = null;
            }
        }

        String url = templateDataObjectCatalogPojo.getRemoteRequestUrl();

        ResponseEntity<String> response = restTemplate.postForEntity(url, remoteCallBase, String.class);

        JSONObject jsonObject = JSON.parseObject(response.getBody());

        return jsonObject;
    }
}
