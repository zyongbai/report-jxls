package com.zyong.report.jxls.remote;

import com.alibaba.fastjson.JSONObject;
import com.zyong.report.jxls.pojo.RemoteCallBase;
import com.zyong.report.jxls.pojo.TemplateDataObjectCatalogPojo;

public interface RemoteCallService {
    JSONObject postRequestCall(TemplateDataObjectCatalogPojo templateDataObjectCatalogPojo, RemoteCallBase remoteCallBase);
}
