package com.zyong.report.jxls;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ReportJxlsSpringbootApplication
 *
 * @author zyong
 * @date 2019/12/22
 */
@MapperScan("com.zyong.report.jxls.dao")
@SpringBootApplication
public class ReportJxlsSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReportJxlsSpringbootApplication.class, args);
    }

}

