package com.zyong.report.jxls.pojo;

import lombok.Data;
import java.util.Date;

@Data
public class TemplateDataObjectLibraryPojo {
    /** 模板ID */
    private String serialNo;
    /** 模板ID */
    private String templateId;
    /** 入参:request;出参:response */
    private String dataType;
    /** 字段 */
    private String colKey;
    /** 字段描述 */
    private String colKeyName;
    /** 字段的全限定类型 */
    private String keyClassType;
    /** 排序(主要用于入参,dataType为request时,保证参数顺序) */
    private Integer sort;
    /** 创建时间 */
    private Date createTime;
    /** 更新时间 */
    private Date updateTime;
}
