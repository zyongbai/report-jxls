package com.zyong.report.jxls.pojo;

import lombok.Data;
import java.util.Date;

@Data
public class TemplateDataObjectCatalogPojo {
    /** 流水号 */
    private String serialNo;
    /** 模板ID */
    private String templateId;
    /** excel模板中数据对象对应的key */
    private String dataKey;
    /** 远程返回数据中列表数据对应的key */
    private String dataResponseKey;
    /** 返回数据转换类型:Bean;Map */
    private String transferType;
    /** 远程数据源请求地址 */
    private String remoteRequestUrl;
    /** 请求类型:GET,POST */
    private String remoteRequestType;
    /** 创建时间 */
    private Date createTime;
    /** 更新时间 */
    private Date updateTime;
}
